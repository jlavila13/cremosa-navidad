<?php

	//no-cache
	header('Cache-Control: no-cache, must-revalidate, public');
	header('Pragma: no-cache');
	//binary file
	//header('Content-Transfer-Encoding: binary');
	//header('Content-Security-Policy "default-src 'self'"');
	//header('Public-Key-Pins "pin-sha256=\"base64+primary==\"; pin-sha256=\"base64+backup==\"; max-age=5184000; includeSubDomains"');
	//header('X-Content-Type-Options: nosniff');
	header('X-Frame-Options: DENY');
	header('X-XSS-Protection: 1; mode=block');

?>
<!DOCTYPE html>
<html lang="en">

<?php include_once("header.php"); ?>
<body>
	<!-- <script src="js/analytics.js"></script> -->

	<main>

		<section class="section-principal">
			<!-- Copy de dinàmica-->
			<article class="copy-principal">
					<p class="text-center"><span class="text-black">ENVÍA</span> <br> <span class="text-light">A TUS SERES QUERIDOS</span><br>
					<i class="fa fa-caret-right d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-right d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-right d-none d-sm-inline" aria-hidden="true"></i>
					<span class="text-mlight text-mlight-mobile">UN GESTO DE CARI&Ntilde;O</span></span><!--  <span class="text-black-regalo">#ELREGALOPERFECTO</span> -->
					<i class="fa fa-caret-left d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-left d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-left d-none d-sm-inline" aria-hidden="true"></i>
					<br>
					<span class="text-light-viñeta">EN 2 SENCILLOS PASOS:</span></p>

					<figure class="text-center img-fluid arrow-down">
						<img src="img/arrow-down.png" alt="Flecha Abajo">
					</figure>

			</article>

		</section>

		<section class="container-fluid mb-4">
			<article class="row">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
				  <div class="carousel-inner" role="listbox">
				    <div class="carousel-item active">
							<!-- <img class="d-block img-fluid" src="img/Steps.png" alt="First slide" style="margin:0 auto;"> -->
							<div class="parrafo">
								<span> <strong>1</strong> <br>paso</span>
								<p>Escribe en el cuadro de texto el nombre de esa persona a quien le vas a dedicar este chocolate</p>
							</div>
							<div class="parrafo">
								<span> <strong>2</strong> <br>paso</span>
								<p>Pulsa el botón para compartir en Facebook o descarga una historia para Instagram</p>
							</div>
				    </div>
				  
				  </div>

			

				</div>
			</article>

		</section>


		<!-- canvas del chocolate -->
		<article class="container-canvas">
			<canvas class="chocoCanvas" id="miCanvas" style="display: none; ">
			</canvas>

			<div class="loader">
				<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
				<span class="sr-only">Loading...</span>
			</div>

			<canvas class="imgGenContainer container" id="chocoName" style="position: relative;">
			</canvas>

			<figure class="imgGenContainer container" style="position: relative; display: none;">

				<img src="img/Chocolate.png" class="choco1 img-fluid" alt="Chocolate centro" id="pintadoTexto">

				<img src="" alt="" id="imgGenerada" class="img-fluid mt-5">

				<div class="pintadoTexto" style=" font-family: Amarillo !important;top: 34.3%; transform: translateY(-50%); position: absolute; left: 0; right: 0; margin: 0 auto; /* width: auto; */ text-align: center; text-transform: capitalize; text-align: center; color: white !important;">

				</div>
				<!-- <input type="text" id="pintadoTexto" value=""> -->
			</figure>
		</article>

		<!-- Imagen generada por canvas -->

		<form class="container">
			<div class="row">

				<input type="text" name="inputChoco" class="form-control col-10 offset-1 offset-sm-1 col-sm-9 col-md-7 form-control-md inputChoco" id="inputChoco" maxlength="15" placeholder="Máximo 15 caracteres.">

				<button type="submit" class="btn btn-gris btn-md col-10 offset-1 offset-sm-0 col-sm-1 col-md-3 vistaPrevia" id="vistaPrevia" formmethod="POST" onClick=”_gaq.push([‘_trackEvent’, ‘External Link’, ‘vista previa’, ‘generar – Words’]);><i class="fa fa-eye " aria-hidden="true"></i> <span class="d-inline d-sm-none d-md-inline" >Vista previa</span></button>

			</div>
		</form>

		<div class="share-container container" style="margin-top: 10%; ">

			<div class="row">
				<a class="btn btn-info col-10 mt-1 offset-sm-4 col-sm-6 col-md-12 offset-md-0" id="guardaCanvas" href="" download="instagram-history.png"><i class="fa fa fa-download pr-2" aria-hidden="true"></i>Descargar</a>
			</div>

			<div class="row">
				<button type="button" id="shareBtn" class="btn btn-primary mt-1 input-sm col-10 offset-sm-4 col-sm-6 col-md-12 offset-md-0"><i class="fa fa-facebook-official pr-2" aria-hidden="true"></i>Compartir</button>
			</div>
		</div>

		<div class="mt-5 contenedor-copyfinal">
				<p class="text-mlight text-center">Una vez que compartas <span class="text-black">#ElRegaloPerfecto </span> <br>
				sigue nuestras redes para que te enteres de esta sorpresa</p>

				<span class="rrss-container text-black text-center">
					<a href="https://www.facebook.com/savoynestle/" target="_blank"><i class="fa fa-facebook pr-2" aria-hidden="true"></i><span class="rrss-text" style="display: none;">Savoy Nestle</span></a>

					<a href="https://twitter.com/NestleSavoy" target="_blank"><i class="fa fa-twitter pr-1" aria-hidden="true"></i><span style="display: none;" class="rrss-text">NestleSavoy</span></a>

					<a href="https://www.instagram.com/savoynestle/?hl=es" target="_blank"><i class="fa fa-instagram pl-1 pr-1" aria-hidden="true"></i><span class="rrss-text" style="display: none;">@savoynestle</span></a>
				</span>
		</div>
	</main>

	<!-- Filtro de palabras -->
	<script src="js/filtro.js"></script>

	<!-- Canvas -->
	<script src="js/canvas.js"></script>
	<!-- Fin Canvas -->
	<figure style="position: relative;" class="d-none d-md-block">
		<img src="img/pestana-nestle.png" alt="Nestle Savoy" style="position: fixed; bottom: 0; right: 0; bottom: 0px; max-width: 25%;">
	</figure>
	
	
	<script>
		// share 
		document.getElementById('shareBtn').onclick = function() {
				var cover = $('meta[name=cover]').attr('content');
				console.log(cover);
				var BASE_URL = window.location.href ;
			 	FB.ui({
							app_id: '323224598146779',
							display: 'popup',
							method: 'share',
							// method: 'share_open_graph',
							action_type: 'og.shares',
							hashtag: '#ElRegaloPerfecto',
							action_properties: JSON.stringify({
								object : {
									'og:url': BASE_URL, 
									'og:title': 'Cremosa Navidad',
									'og:site_name': 'SAVOY',
									'og:url': 'http://savoy.com.ve/cremosa-navidad',
									'og:description': 'Porque estemos donde estemos, quiero que juntos compartamos esta cremosa navidad #ElRegaloPerfecto',
									'og:image': cover,
									'og:image:width': '1200',
									'og:image:height': '630',
								}
						})
				});
				
		};
	</script>
	<div class="bg"></div>
</body>
</html>
