$(document).ready(function(){

		$("#guardaCanvas").click(function(){
				
			if($('#inputChoco').val() == ''){
				alert('No dejes el nombre vacío');
				return false;
			}

		});

		// $('[data-toggle="tooltip"]').tooltip();

		//Filtro de numeros, no permite ingresar teclas numéricas ni comillas, espacios.
		$("#inputChoco").on("keydown", function(event){
		  // Allow controls such as backspace, tab etc.
		  var arr = [8,9,16,17,20,35,36,37,38,39,40,45,46];

		  // Allow letters
		  for(var i = 65; i <= 90; i++){
		  	arr.push(i);
		  }

		  // Prevent default if not in array
		  if(jQuery.inArray(event.which, arr) === -1){
		  	event.preventDefault();
		  }
		});

		$("#inputChoco").on("input", function(){
			var regexp = /[^a-zA-Z]/g;
			if($(this).val().match(regexp)){
				$(this).val( $(this).val().replace(regexp,'') );
			}
			fondo();

		});

		//Ésta función arma el canvas con los parámetros, textoChoco que es el texto del input
		//y Tamano choco que determina el tamano del texto basado en el viewport y la cantidad
		//de letras
		function canvas(textoChoco, tamanoChoco){
			//Variables globales para las imagenes

			var imgWidth, imgHeight;
			var canvas = document.getElementById('miCanvas');

			//Compruebo si existe el objeto canvas, y escribo su contexto

			if (canvas.getContext){

				//Botón que genera copia del Canvas, oculta para hacer la ilusión de descarga

				$("#guardaCanvas").click(function(e){
					guardarImagen(canvas);
				});
				$("#guardaCanvasIE").click(function(e){
					subirImagen(canvas);
				});

				//Instancio el objeto image, con la info de la imagen generada
				var img =  new Image();

					//Ruta de la imagen
					// img.src = "img/Chocolate.png";
					img.src = "img/Post-StorieDescargable.png";

					//Cuando se cargue la imagen la dibujo en el lienzo
					img.onload = function()
					{
						//Obtengo el contexto
						var ctx = canvas.getContext("2d");
						imgWidth = this.width;
						imgHeight = this.height;
						canvas.width = imgWidth;
						canvas.height = imgHeight;
						ctx.drawImage(this, 0, 0); // Dibuja los gráficos con 3 parametros obligatorios

						//Para manipular la imagen y obtener información de esta
						var datosImagen = ctx.getImageData(0,0, imgWidth/2, imgHeight/2);
						var imgData = datosImagen.data;
						ctx.beginPath(); //iniciar ruta
						ctx.translate( imgWidth/2, imgHeight/2);
						ctx.rotate(Math.PI/-4.3);
						ctx.strokeStyle="white"; //color externo
						ctx.fillStyle="white"; //color de relleno
						ctx.font= tamanoChoco+" Amarillo"; //estilo de texto
						ctx.textAlign = 'center';
						ctx.fillText(textoChoco, 53,-30); 
						ctx.restore();
					};
				}

				function guardarImagen(link, canvasId, filename) {
					var datosCanvas = canvas.toDataURL();
					var preCanvas = canvas.toDataURL();
					$('#guardaCanvas').attr('href', datosCanvas);
					link.href = document.getElementById('miCanvas').toDataURL();
					link.download = filename;
				}

				// para el mariquita de IE 
				function subirImagen(link, canvasId, filename) {
					var datosCanvas = canvas.toDataURL();
					var preCanvas = canvas.toDataURL();
					$('#guardaCanvas').attr('href', datosCanvas);
					link.href = document.getElementById('miCanvas').toDataURL();
					link.download = filename;
					deerk(datosCanvas); 
				}

			}

			//Esto se dispara al hacer click en el botón de vista previa, oculta los textos y coloca la imagen del canvas.
			$("#vistaPrevia").on('click tap', function(){
				
				var texto = $('#inputChoco').val();
				let tamano;

				if (screen.width < 767) {
					if (texto.length <= 5) {
						tamano = '.7em';
					}else if(texto.length >= 6 && texto.length <= 10){
						tamano = '.8em';
					}else{
						tamano = '.7em';
					}
				}else{
					if (texto.length <= 5) {
						tamano = '28px';
					}else if(texto.length >= 6 && texto.length <= 10){
						tamano = '30px';
					}else{
						tamano = '20px';
					}
				}

				$('.pintadoTexto').html('<input type="text" style="border:none; background:transparent; text-transform:capitalize; height: 100px !important; padding:2%; color: white !important; text-align:center; font-family: Amarillo !important; transform: rotate(-17deg); max-width: 260px; margin-left:2em; font-size: '+tamano+';" disabled value="'+texto+'">');

				if (texto.length <= 5) {
					tamano = '3em';
				}else if(texto.length >= 6 && texto.length <= 10){
					tamano = '2.8em';
				}else{
					tamano = '2.5em';
				}
				
				canvas(texto, tamano);

				//Al ejecutar el vista previa
				$(".copy-principal").show(); //dejo el copy principal
				$(".choco1").show(); //el chocolate
				$("#pintadoTexto").css("color", "white"); // Hago aparecer el texto  en el chocolate
				$("#pintadoTexto").show(); // Hago aparecer el texto  en el chocolate
				console.log(texto.length);

				// nombre en la vista previa
				var name = document.getElementById("chocoName");
				var ctx = name.getContext("2d");
				ctx.font= " 50px Amarillo"; //estilo de texto
				ctx.strokeStyle="white"; //color externo
				ctx.fillStyle="white"; //color de relleno
				ctx.textAlign = "center";
				// ctx.rotate(Math.PI/-20.0); 
				ctx.fillText(texto, 640, 310); 
				ctx.restore();

				openGraph();


			});

});

		function fondo(){
			var name = document.getElementById("chocoName");
			var x = 0;
			var y = 0;
			var width = 1200;
			var height = 630;
			var ctx = name.getContext("2d");
			ctx.beginPath();
			ctx.canvas.height = height;
			ctx.canvas.width  = width;
			ctx.font= " 1px Amarillo";
			ctx.fillText('_lab',400, 200);
			var bg = new Image();
			bg.src = "img/Chocolate.png";
			bg.onload = function(){
				ctx.mozImageSmoothingEnabled = true;
				ctx.webkitImageSmoothingEnabled =true;
				ctx.msImageSmoothingEnabled = true;
				ctx.imageSmoothingEnabled = true;
				ctx.drawImage(bg,x, y, width, height);
			};
			ctx.restore();

		}

		function instFondo(){
			var texto = $('#inputChoco').val();
			var inst = document.getElementById("miCanvas");
			var x = 0;
			var y = 0;
			var width = 750;
			var height = 1334;
			var ctx = inst.getContext("2d");
			ctx.beginPath();
			ctx.canvas.height = height;
			ctx.canvas.width  = width;
			ctx.font= " 1px Amarillo";
			ctx.fillText('_lab',400, 200);
			var bg = new Image();
			bg.src = "img/Post-StorieDescargable.png";
			bg.onload = function(){
				ctx.mozImageSmoothingEnabled = true;
				ctx.webkitImageSmoothingEnabled =true;
				ctx.msImageSmoothingEnabled = true;
				ctx.imageSmoothingEnabled = true;
				ctx.drawImage(bg,x, y, width, height);
			};
			ctx.restore();

		}

		function openGraph(){
			// obteniendo el meta para OG    
			var files = document.getElementById('chocoName').toDataURL().split(',')[1];
			console.log('subiendo a IMGUR');
			var apiUrl = 'https://api.imgur.com/3/image';
			var imgurClientId = 'bda601c27a38c1a';
			$.ajax({
				url: apiUrl,
				type: 'POST',
				headers: {
					Authorization: 'Client-ID ' + imgurClientId,
					Accept: 'application/json'
				},
				data: {
					image: files,
					type: 'base64'
				},
				beforeSend: function () {
					$('#chocoName').css('opacity', '0.4');
					$('.loader').fadeIn();
					$('button').attr('disabled', 'disabled');
				},
			})
			.done(function (result) {
				var url = 'https://i.imgur.com/' + result.data.id + '.png';
				$('.holder').html("<a href='" + url + "'>" + url + "</a>");
				$('meta[name=cover]').attr('content', url);
				console.log('ver en ' + url);
				$('.loader').fadeOut();
				$('#chocoName').css('opacity', '1');
				$('button').removeAttr('disabled');
			});	
		}

		function deerk(insta){
			// descarga para el retardado mental de IE 
			var files = insta.split(',')[1];
			console.log('subiendo a imagen descargable...');
			var apiUrl = 'https://api.imgur.com/3/image';
			var imgurClientId = 'bda601c27a38c1a';
			$.ajax({
				url: apiUrl,
				type: 'POST',
				headers: {
					Authorization: 'Client-ID ' + imgurClientId,
					Accept: 'application/json'
				},
				data: {
					image: files,
					type: 'base64',
				},
				beforeSend: function () {
					$('.downloader').fadeIn();
					$('button').attr('disabled', 'disabled');
				},
			})
			.done(function (result) {
				console.log(result);
				var downUrl = 'https://i.imgur.com/' + result.data.id + '.png';
				var ua = window.navigator.userAgent;
				console.log('descargar de: ' + downUrl);
				var msie = ua.indexOf("MSIE ");
				$('.chocoHolder').attr('src', downUrl);
				$('.c-holder').show();
				$('.downloader').fadeOut();
				$('main').fadeOut();
				$('button').removeAttr('disabled');
			});	
		}

		$(document).ready(function(){
			instFondo();
			fondo();
		});

	


		
	